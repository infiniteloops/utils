﻿using System;
using Xunit;
using InfiniteLoop.Utilities.ExtensionMethods;
using System.Security.Cryptography;

namespace InfiniteLoop.Utilities.Tests
{
    public class HashTests
    {
        [Fact]
        public void HashSameStringUsingDefaults()
        {
            const string password = "test string";
            string salt;
            var algo = SHA256.Create();
            string expected = password.ToHash(algo, out salt);
            string actual = password.ToHash(algo, salt);
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void HashSameStringConvertingSaltToBytes()
        {
            const string password = "test string";
            string salt;
            var algo = SHA256.Create();
            string expected = password.ToHash(algo, out salt);
            string actual = password.ToHash(algo, Convert.FromBase64String(salt));
            Console.WriteLine(salt);
            Console.WriteLine(actual);
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void PreImageHashMatches()
        {
            const string salt = "L8Tlk2HAXprwrXGKGpOiIDgocJyrVZsQ4b0edo6Bep0Bosic9YocylPZ6g9nr7A2sS56KFJt5Nop1LGkoegMbjLUiXfLBiutC6ZI0WqLqTIM6MVfBlJKk73tcyDZeChrzXzckiU4aqsDzRyrJRNvnSp/l7gjrGjgifa63ZIK05M=";
            const string expected = "tBwUdFLqQO1aI8kN7RQ/nsqi3lXLsr7BDdb1xemKQCY=";
            const string password = "test string";            
            var algo = SHA256.Create();
            string actual = password.ToHash(algo, Convert.FromBase64String(salt));
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void HashDifferentStringsUsingDefaults()
        {
            const string password = "test string";
            const string password2 = "test2 string";
            string salt;
            var algo = SHA256.Create();
            string expected = password.ToHash(algo, out salt);
            string actual = password2.ToHash(algo, salt);
            Assert.NotEqual(expected, actual);
        }

        [Fact]
        public void HashStringUsingDifferentIterationCount()
        {
            const string password = "test string";
            string salt;
            var algo = SHA256.Create();
            string expected = password.ToHash(algo, 10, out salt);
            string actual = password.ToHash(algo, 11, salt);
            Assert.NotEqual(expected, actual);
        }
    }
}
