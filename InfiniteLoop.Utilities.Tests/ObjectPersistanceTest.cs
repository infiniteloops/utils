﻿using System.Collections.Generic;
using Xunit;
using System.IO;

namespace InfiniteLoop.Utilities.Tests {
	/// <summary>
	/// Summary description for UnitTest1
	/// </summary>
	public class ObjectPersistanceTest {
		/// <summary>
		///A test for SaveToXml
		///</summary>
		public void SaveToXmlTestHelper<T>()
			where T : class , new() {
			var objectToSave = new T(); // TODO: Initialize to an appropriate value
			const string filePath = @"c:\temp\test.xml"; // TODO: Initialize to an appropriate value
		    bool actual = ObjectPersistance.SaveToFile(objectToSave, filePath);
			Assert.Equal(true, actual);				
		}

		[Fact]
		public void SaveToXmlTest() {
			SaveToXmlTestHelper<List<string>>();
		}

		/// <summary>
		///A test for GetFromXml
		///</summary>
		public void GetFromXmlTestHelper<T>()
			where T : class , new() {
			const string filePath = @"c:\temp\test.xml"; // TODO: Initialize to an appropriate value
			var expected = new T(); // TODO: Initialize to an appropriate value
		    var actual = ObjectPersistance.GetFromFile<T>(filePath);
		    using (var file = new FileInfo(filePath).OpenRead())
		    {
		        var streamTest = ObjectPersistance.GetFromStream<T>(file);
		        Assert.Equal(expected, actual);
		        Assert.Equal(streamTest, actual);
		    }
			}

		[Fact]
		public void GetFromXmlTest() {
			GetFromXmlTestHelper<List<string>>();
		}
	}
}
