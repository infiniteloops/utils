﻿using System;
using System.Linq;
using System.Text;
using System.Security.Cryptography;

namespace InfiniteLoop.Utilities.ExtensionMethods
{
	public static class HashHelper
	{
		/// <summary>
		/// Hash the given string using the provided hashalgoritm and salt
		/// </summary>
		/// <param name="value"></param>
		/// <param name="algorithm">SHA 256 or above recommended</param>
		/// <param name="salt">salt as byte array</param>
		/// <returns></returns>
		public static string ToHash(this string value, HashAlgorithm algorithm, byte[] salt) {
			byte[] clearString = Encoding.UTF8.GetBytes(value);
		    byte[] saltedString = salt != null ? clearString.Concat(salt).ToArray() : clearString;
			byte[] hashString = algorithm.ComputeHash(saltedString);
			return Convert.ToBase64String(hashString);
		}

		/// <summary>
		/// Hash the given string using the provided hashalgoritm and output a randomly generated 128bit salt
		/// </summary>
		/// <param name="value"></param>
		/// <param name="algorithm">SHA 256 or above recommended</param>
		/// <param name="salt">Randomly generated 128bit salt</param>
		/// <returns></returns>
		public static string ToHash(this string value, HashAlgorithm algorithm, out string salt) {
			byte[] clearString = Encoding.UTF8.GetBytes(value);
		    var saltArray = new byte[128];
			var r = new Random();
			r.NextBytes(saltArray);
			byte[] saltedString = clearString.Concat(saltArray).ToArray();
			salt = Convert.ToBase64String(saltArray);
			byte[] hashString = algorithm.ComputeHash(saltedString);
			return Convert.ToBase64String(hashString);
		}

		/// <summary>
		/// Hash the given string using the provided hashalgoritm and previously generated 128bit salt
		/// </summary>
		/// <param name="value"></param>
		/// <param name="algorithm">SHA 256 or above recommended</param>
		/// <param name="salt">Randomly generated 128bit salt</param>
		/// <returns></returns>
		public static string ToHash(this string value, HashAlgorithm algorithm, string salt) {
			byte[] clearString = Encoding.UTF8.GetBytes(value);
		    byte[] saltArray = Convert.FromBase64String(salt);			
			byte[] saltedString = clearString.Concat(saltArray).ToArray();
			byte[] hashString = algorithm.ComputeHash(saltedString);
			return Convert.ToBase64String(hashString);
		}

		/// <summary>
		/// Hash the given string using the provided hashalgoritm, this is salted and streched the number of times specified by iteration count. Output salt is the randomly generated 128bit salt.
		/// </summary>
		/// <param name="value"></param>
		/// <param name="algorithm">SHA 256 or above recommended</param>		
		/// <param name="iterationCount">Number of times to strech the key</param>
		/// <param name="salt">Randomly generated 128bit salt</param>
		/// <returns></returns>
		public static string ToHash(this string value, HashAlgorithm algorithm, int iterationCount, out string salt) {
			byte[] clearString = Encoding.UTF8.GetBytes(value);
		    var saltArray = new byte[128];
			var r = new Random();
			r.NextBytes(saltArray);
			byte[] saltedString = clearString.Concat(saltArray).ToArray();
			salt = Convert.ToBase64String(saltArray);
			byte[] hashString = algorithm.ComputeHash(saltedString);
			for (int i = 0; i < iterationCount; i++) {
				hashString = hashString.Concat(clearString).Concat(saltArray).ToArray();
				hashString = algorithm.ComputeHash(hashString);
			}
			return Convert.ToBase64String(hashString);
		}

		/// <summary>
		/// Hash the given string using the provided hashalgoritm, this is salted and streched the number of times specified by iteration count.
		/// </summary>
		/// <param name="value"></param>
		/// <param name="algorithm">SHA 256 or above recommended</param>		
		/// <param name="iterationCount">Number of times to strech the key</param>
		/// <param name="salt">Byte Array contain Salt value</param>
		/// <returns></returns>
		public static string ToHash(this string value, HashAlgorithm algorithm, int iterationCount, byte[] salt)
		{
			byte[] clearString = Encoding.UTF8.GetBytes(value);
		    byte[] saltedString = clearString.Concat(salt).ToArray();
			byte[] hashString = algorithm.ComputeHash(saltedString);
			for (int i = 0; i < iterationCount; i++)
			{
				hashString = hashString.Concat(clearString).Concat(salt).ToArray();
				hashString = algorithm.ComputeHash(hashString);
			}
			return Convert.ToBase64String(hashString);
		}

		/// <summary>
		/// Hash the given string using the provided hashalgoritm, this is salted and streched the number of times specified by iteration count.
		/// </summary>
		/// <param name="value"></param>
		/// <param name="algorithm">SHA 256 or above recommended</param>		
		/// <param name="iterationCount">Number of times to strech the key</param>
		/// <param name="salt">Randomly generated 128bit salt</param>
		/// <returns></returns>
		public static string ToHash(this string value, HashAlgorithm algorithm, int iterationCount, string salt) {
			byte[] clearString = Encoding.UTF8.GetBytes(value);
		    byte[] saltArray = Convert.FromBase64String(salt);
			byte[] saltedString = clearString.Concat(saltArray).ToArray();
			byte[] hashString = algorithm.ComputeHash(saltedString);
			for (int i = 0; i < iterationCount; i++) {
				hashString = hashString.Concat(clearString).Concat(saltArray).ToArray();
				hashString = algorithm.ComputeHash(hashString);
			}
			return Convert.ToBase64String(hashString);
		}
	}	    
}
