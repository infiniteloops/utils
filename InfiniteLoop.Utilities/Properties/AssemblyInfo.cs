﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("InfiniteLoop.Utilities")]
[assembly: AssemblyDescription("Generic Utilities")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Infinite Loops")]
[assembly: AssemblyProduct("InfiniteLoops Utilities")]
[assembly: AssemblyCopyright("")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
[assembly: ComVisible(false)]
[assembly: Guid("4a75d4b9-a92c-479b-9ebe-b600372ceca1")]
[assembly: AssemblyVersion("1.2.*")]
[assembly: AssemblyFileVersion("1.0.0.0")]
