﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.IO;
using System.Xml.Serialization;

namespace InfiniteLoop.Utilities
{
    public class XMLHelpers
    {
        /// <summary>
        /// Saves the provided collection of <typeparamref name="T"/> to the given file
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="resourceObject"></param>
        /// <param name="filePath"></param>
        public static void SaveObject<T>(T resourceObject, string filePath)
        where T : class, IEnumerable
        {
            FileStream myFile = null;
            try
            {
                myFile = new FileStream(filePath, FileMode.OpenOrCreate);
                var resourceXml = new XmlSerializer(typeof(T));
                resourceXml.Serialize(myFile, resourceObject);
            }
            finally
            {
                if (myFile != null)
                {
                    myFile.Close();
                }
            }
        }

        /// <summary>
        /// Retrieve the object from the given file path
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public static T GetObject<T>(string filePath)
            where T : class, IEnumerable, new()
        {
            object obj = new T();

            if (File.Exists(filePath))
            {
                var myFile = new FileStream(filePath, FileMode.Open);
                var currentData = new XmlSerializer(typeof(T));
                obj = currentData.Deserialize(myFile);
            }

            return obj as T;
        }
    }
}
