﻿using System;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace InfiniteLoop.Utilities
{
    public static class ObjectPersistance
    {
        #region Serialize

        /// <summary>
        ///     Persist object to xml file
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="objectToSave"></param>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public static bool SaveToFile<T>(T objectToSave, string filePath)
            where T : class, new()
        {
            bool successful = true;
            FileStream fs = null;
            try
            {
                using (fs = new FileStream(filePath, FileMode.Create))
                {
                    Serialize(objectToSave, fs);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
                successful = false;
                if (fs != null)
                {
                    fs.Close();
                }
            }

            return successful;
        }

        /// <summary>
        ///     Persist object to xml string
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="objectToSave"></param>
        /// <returns></returns>
        public static string SaveToXmlString<T>(T objectToSave)
            where T : class, new()
        {
            var resultBuilder = new StringBuilder();
            using (XmlWriter xw = XmlWriter.Create(resultBuilder))
            {
                var serializer = new XmlSerializer(typeof (T));
                serializer.Serialize(xw, objectToSave);
            }

            return resultBuilder.ToString();
        }

        /// <summary>
        ///     Persist object to XDocument
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="objectToSave"></param>
        /// <returns></returns>
        public static XDocument SaveToXDocument<T>(T objectToSave)
            where T : class, new()
        {
            var ms = new MemoryStream();
            Serialize(objectToSave, ms);
            XDocument xdoc = XDocument.Load(ms);
            return xdoc;
        }

        /// <summary>
        ///     Serialize object to stream
        /// </summary>
        /// <typeparam name="T1"></typeparam>
        /// <typeparam name="T2"></typeparam>
        /// <param name="objectToSave"></param>
        /// <param name="stream"></param>
        public static void Serialize<T1, T2>(T2 objectToSave, T1 stream)
            where T1 : Stream
            where T2 : class, new()
        {
            var serializer = new XmlSerializer(typeof (T2));
            serializer.Serialize(stream, objectToSave);
        }

        #endregion

        #region Deserialize

        /// <summary>
        ///     Load object from file
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public static T GetFromFile<T>(string filePath)
            where T : class, new()
        {
            object retObject;

            if (File.Exists(filePath))
            {
                using (var fileStream = new FileStream(filePath, FileMode.Open))
                {
                    retObject = GetFromStream<T>(fileStream);
                }
            }
            else
            {
                throw new FileNotFoundException(string.Format("{0} was not found, please make sure this file exists.",
                                                              filePath));
            }

            return retObject as T;
        }

        /// <summary>
        ///     Load object from XML string
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="xmlData"></param>
        /// <returns></returns>
        public static T GetFromXmlString<T>(string xmlData)
            where T : class, new()
        {
            object retObject = new T();

            var serializer = new XmlSerializer(typeof (T));

            if (!string.IsNullOrEmpty(xmlData))
            {
                using (XmlReader reader = XmlReader.Create(new StringReader(xmlData)))
                {
                    retObject = serializer.Deserialize(reader);
                }
            }

            return retObject as T;
        }

        /// <summary>
        ///     Load object from XDocument
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="xdoc"></param>
        /// <returns></returns>
        public static T GetFromXDocument<T>(XDocument xdoc)
            where T : class, new()
        {
            var serializer = new XmlSerializer(typeof (T));
            object retObject = serializer.Deserialize(xdoc.CreateReader());

            return retObject as T;
        }

        /// <summary>
        ///     Hydrate object from stream
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="stream"></param>
        /// <returns></returns>
        public static T GetFromStream<T>(Stream stream)
            where T : class, new()
        {
            var serializer = new XmlSerializer(typeof (T));
            object retObject = serializer.Deserialize(stream);

            return retObject as T;
        }

        #endregion
    }
}