﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Threading.Tasks;

namespace InfiniteLoop.Utilities
{
    /// <summary>
    ///     All items are validated before being allowed into the current collection
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public sealed class ValidatedCollection<T> : Collection<T>
        where T : class
    {
        /// <summary>
        ///     This delegate is used to allow the implementer the ability to handle validation errors as they occur
        /// </summary>
        public Action<T, List<ValidationResult>> OnValidationFailed;

        protected override void InsertItem(int index, T item)
        {
            // comment
            TryValidateItem(index, item, base.InsertItem, OnValidationFailed);
        }

        protected override void SetItem(int index, T item)
        {
            TryValidateItem(index, item, base.SetItem, OnValidationFailed);
        }

        // Open Question: is Dictionary<Tkey, Tvalue> useful here or would List<CustomClass> be better
        // T is known by the consumer and List<validationresults> is a base type so would introducing a new type gain anything
        // Dictionarys are used to associated keys with values, while that is part of what we are trying to do mainly it is just trying to get the data back to the consumer
        // A dictionary here seems like the wrong construct
        // Other options?
        public IDictionary<T, List<ValidationResult>> ValidateCollection()
        {
            var results = new ConcurrentDictionary<T, List<ValidationResult>>();
            Action<T, List<ValidationResult>> onError = (valItem, valResult) => results.TryAdd(valItem, valResult);
            Parallel.ForEach(this, item =>
            {
                int idxItem = this.IndexOf(item);
                TryValidateItem(idxItem, item, null, onError);
            });

            return results;
        }

        /// <summary>
        ///     This method will try to validation the item passed and will run the successMethod on success
        ///     Runs the errorMethod on failure
        /// </summary>
        /// <param name="index"></param>
        /// <param name="item"></param>
        /// <param name="successMethod"></param>
        /// <param name="errorMethod"></param>
        private void TryValidateItem(int index, T item, Action<int, T> successMethod,
                                       Action<T, List<ValidationResult>> errorMethod)
        {
            var results = new List<ValidationResult>();
            if (Validator.TryValidateObject(item, new ValidationContext(item, null, null), results, true))
            {
                // ensure success delegate exists before calling, this is not required as we can validate in memory objects
                // without needing any method to fire if they are valid
                if (successMethod != null)
                {
                    successMethod(index, item);
                }
            }
            else if (errorMethod != null)
            {
                errorMethod(item, results);
            }
            else
            {
                throw new ValidationException(results.ToResultsString());
            }
        }
    }

    public static class ValidationResultsExtensions
    {
        public static string ToResultsString(this List<ValidationResult> valResults)
        {
            var valStringBuilder = new StringBuilder();

            Parallel.ForEach(valResults, valResult =>
            {
                var valLine = new StringBuilder();
                valLine.Append(valResult.ErrorMessage);
                valLine.Append(" [ ");
                valLine.Append(string.Join(", ", valResult.MemberNames));
                valLine.Append(" ]");
                valStringBuilder.AppendLine(valLine.ToString());
            });

            return valStringBuilder.ToString();
        }
    }
}